#pragma once
#include "Card.h"
#include "Player.h"
#include <vector>
#include <random>
#include <time.h>
#include <algorithm>
#include <cstdlib>
#include <ctime>
#include <algorithm>

class Board
{

	std::vector<Card>cartile_de_jos;
	std::vector<Card>deck;
	std::vector<Player>players;
	std::vector<int>scoruri;

public:
	std::vector<vector<int>>echipe;
	int sunt_echipe;
	int nr_playeri;
	std::vector<int>scoruri_echipe;

	//constexpr int deck_size = 54;
	Board();

	void AlegeTip();

	void SetEchipe();
	int GetNrPlayeri();
	void Deck();
	void ShufflePachet();
	void ShowCards();
	void Trage(Player& player);
	void SetPlayers();
	void ImparteCartile();
	void NrCartiPachet();
	void Refuz();
	void Tabla();
	void AfisareMaini();
	void AfisareBoard();
	void DropCard(int nr_player, int element);
	void DeckTOboard(int nr_carti);
	void PrintPairs(std::vector<Card>arr, int arr_size, int sum, int poz);
	void PerechiMana(int nr_player);
	void BiggestScorePlayer();
	void Puncte(int cartea_din_mana, std::vector<int>& deLuat, int nr_player);
	void Calcul(Player player);
	void removeBoardCard(int element);
	int getNrMaini(int nr_player);
	bool CheckSuma(std::vector<int>elemente, int nr_player, int nr);
	int Verificare(std::vector<Card>arr, int arr_size, int sum, int poz);
	int GetPlayerCardValue(Player player, int nr);
	int ExistaCarti(int nr_player);
	void Trage6Carti(int nr_player);
	int StillPlayable();
	int GetDeckSize();
	void AdaugaCarteExtra();
	void AdaugaCarteExtra2(int nr);
	int CeleMaiMulteCarti();
	int GetSizeCartiJos();

	void Winner();


};

