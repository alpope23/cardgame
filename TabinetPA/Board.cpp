﻿#include "Board.h"
#include "Player.h"
#include "Card.h"
#include <cstdlib>
#include <ctime>
#include <algorithm>
#include <vector>
#include <unordered_set>



Board::Board()
{

}

void Board::Deck()
{

	for (int index = 1; index < 14; index++)
	{
		Card carte(index + 1, T);
		deck.push_back(carte);
	}
	for (int index = 1; index < 14; index++)
	{
		Card carte(index + 1, IN);
		deck.push_back(carte);
	}
	for (int index = 1; index < 14; index++)
	{
		Card carte(index + 1, IR);
		deck.push_back(carte);
	}
	for (int index = 1; index < 14; index++)
	{
		Card carte(index + 1, R);
		deck.push_back(carte);
	}

}


void Board::Tabla()
{
	for (int index = 0; index < 52; index++)
	{
		Card carte(NULL, NULL);
		cartile_de_jos.push_back(carte);
	}


}
int Board::GetNrPlayeri()
{
	return players.size();
}


void Board::ShowCards()
{
	for (int i = 0; i < deck.size(); i++)
	{
		std::wcout << "Cartea cu numarul: " << i + 1 << " este: " << endl;
		std::wcout << " ____" << endl;
		std::wcout << "|    |" << endl;
		std::wcout << "| ";
		deck[i].AfisareCarte();
		std::wcout << " |" << endl;
		std::wcout << "|____|";
		std::wcout << endl;
	}

}

int Board::getNrMaini(int nr_player)
{
	int nr = 0;
	for (int i = 0; i < players[nr_player].cards.size(); i++)
	{
		int suma = players[nr_player].cards[i].GetValue();
		if (Verificare(cartile_de_jos, cartile_de_jos.size(), suma, i) == 1)
		{
			nr++;
		}
	}
	for (int i = 0; i < players[nr_player].cards.size(); i++)    // Cazul 1:1 
	{
		int suma = players[nr_player].cards[i].GetValue();
		for (int j = 0; j < cartile_de_jos.size(); j++)
		{
			if (suma == cartile_de_jos[j].GetValue())
				nr++;
		}
	}
	return nr;

}
void Board::PerechiMana(int nr_player)
{
	int nr = 0;
	for (int i = 0; i < players[nr_player].cards.size(); i++)
	{
		int suma = players[nr_player].cards[i].GetValue();
		if (Verificare(cartile_de_jos, cartile_de_jos.size(), suma, i) == 1)
		{
			PrintPairs(cartile_de_jos, cartile_de_jos.size(), suma, i);
		}
	}

}



int Board::CeleMaiMulteCarti()
{
	int max = 0;
	for (int i = 0; i < players.size(); i++)
	{
		if (players[i].gramada.size() > max)
			max == players[i].gramada.size();
	}
	return max;
}
void  Board::AdaugaCarteExtra()
{
	for (int i = 0; i < players.size(); i++)
		if (players[i].gramada.size() == CeleMaiMulteCarti())
		{
			Card carte(100, IR);
			players[i].gramada.push_back(carte);
		}


}
void  Board::AdaugaCarteExtra2(int nr)
{
	Card carte(99, IR);
			players[nr].gramada.push_back(carte);


}
int Board::GetSizeCartiJos()
{
	return cartile_de_jos.size();
}
void Board::Winner()
{
	AdaugaCarteExtra();
	int new_score;
	int max = 0;
	if (sunt_echipe == 0)
	{
		for (int i = 0; i < echipe.size(); i++)
		{
			int suma = 0;

			for (int j = 0; j < echipe[i].size(); j++)
			{
				suma = suma + players[echipe.size() * i + j].GetScore();

			}
			scoruri_echipe.push_back(suma);
			if (suma > max)
				max = suma;
		}
		for (int i = 0; i < echipe.size(); i++)
		{
			int suma = 0;

			for (int j = 0; j < echipe[i].size(); j++)
			{
				suma = suma + players[echipe.size() * i + j].GetScore();
			}
			if (suma == max)
				std::wcout << "             Echipa castigatoare este: " << i + 1;
		}

	}

	else
		BiggestScorePlayer();
}
void Board::BiggestScorePlayer()
{

	int new_score;
	for (int i = 0; i < players.size(); i++)
	{
		new_score = players[i].GetScore();
		scoruri.push_back(new_score);

	}

	int max = 0;
	for (int i = 0; i < players.size(); i++)
	{
		if (players[i].GetScore() > max)
		{
			max = players[i].GetScore();
		}
	}
	for (int i = 0; i < players.size(); i++)
	{
		if (players[i].GetScore() == max)
		{
			std::wcout << "               Castigatorul este jucatorul " << i + 1;
		}
	}

}


void Board::PrintPairs(std::vector<Card>arr, int arr_size, int sum, int poz)
{

	unordered_set<int> s;
	for (int i = 0; i < arr_size; i++)
	{
		int temp;
		temp = sum - arr[i].GetValue();


		if (s.find(temp) != s.end() && temp > 0)
			std::wcout << " [" << sum << "]-->" << poz;
		s.insert(arr[i].GetValue());
	}
}
int Board::Verificare(std::vector<Card>arr, int arr_size, int sum, int poz)
{

	unordered_set<int> s;
	for (int i = 0; i < arr_size; i++)
	{
		int temp;
		temp = sum - arr[i].GetValue();


		if (s.find(temp) != s.end() && temp > 0)
			return 1;
		s.insert(arr[i].GetValue());
	}
	return 0;
}


void Board::ShufflePachet()
{
	srand(time(NULL));
	for (int k = 0; k < deck.size(); k++)
	{
		int r = k + rand() % (deck.size() - k);
		swap(deck[k], deck[r]);
	}
}
void Board::Trage(Player& player)
{
	player.AddCard(deck[deck.size() - 1]);
	deck.pop_back();

}

void Board::AlegeTip()
{
	int tip;
	std::wcout << "Doriti echipe sau individual?   [0] Echipe  [1] Individual";
	std::cin >> tip;
	sunt_echipe = tip;
	if (sunt_echipe == 0)
		SetEchipe();
	else
		SetPlayers();
}

void Board::SetEchipe()
{
	int input;
	int input_nr;
	std::wcout << "Cate echipe doriti? [2/3/4]";
	std::cin >> input;
	if (input == 2 || input == 3 || input == 4)
	{
		for (int index = 0; index < input; index++)
		{
			std::vector<int>nou;
			echipe.push_back(nou);
			SetPlayers();
			std::cin >> input_nr;
			if (input_nr == 2 || input_nr == 3 || input_nr == 4)
			{
				for (int index2 = 0; index2 < input_nr; index2++)
				{
					echipe[index].push_back(index2);
				}
			}
			else
			{
				std::wcout << "Numarul de playeri este invalid, incercati din nou! ";
				SetEchipe();
			}

		}
	}
	else
	{
		std::wcout << "Numarul de echipe este invalid, incercati din nou! ";
		SetEchipe();
	}



}
void Board::SetPlayers()
{
	int x;
	std::wcout << "Cati playeri doresti in acest joc/echipa?   [2/3/4]  ";
	cin >> x;
	if (x == 2 || x == 3 || x == 4)
	{
		for (int i = 0; i < x; i++)
		{
			nr_playeri++;

		}
	}
	else
	{
		std::wcout << "Numarul de playeri este invalid, incercati din nou! ";
		SetPlayers();
	}


}

void Board::NrCartiPachet()
{
	std::wcout << " Mai sunt " << deck.size() << " carti in pachet";

}
int Board::GetDeckSize()
{

	return deck.size();
}

void Board::AfisareBoard()
{

	for (int i = 0; i < cartile_de_jos.size(); i++)
	{
		std::wcout << "Cartea cu numarul: " << i << " este: " << endl;
		std::wcout << " ____" << endl;
		std::wcout << "|    |" << endl;
		std::wcout << "| ";
		cartile_de_jos[i].AfisareCarte();
		std::wcout << " |" << endl;
		std::wcout << "|____|";
		std::wcout << endl;
	}


}

void Board::ImparteCartile()
{

	AlegeTip();
	for (int i = 0; i < nr_playeri; i++)
	{

		Player newPlayer;
		newPlayer.SetDataPlayer();
		for (int j = 0; j < 4; j++)
		{
			Trage(newPlayer);


		}
		players.push_back(newPlayer);


	}

}
void Board::removeBoardCard(int element)
{
	cartile_de_jos.erase(cartile_de_jos.begin() + element);

}
void Board::AfisareMaini()
{
	for (int i = 0; i < nr_playeri; i++)
	{
		std::wcout << L"╔═══════════════════════════════════════════════════════════════════════════════════════════════════════════════════════╗" << endl;
		std::wcout << L"║" << "                                       Cartile jucatarului " << i  << " sunt: " << endl;
		players[i].ShowCards();
		std::wcout << L"╚═══════════════════════════════════════════════════════════════════════════════════════════════════════════════════════╝" << endl;

	}


}
int Board::StillPlayable()
{
	int OK = 0;
	for (int i = 0; i < players.size(); i++)
	{
		if (ExistaCarti(i) == 0 && deck.size() == 0)
		{
			OK++;
		}
	}
	return OK;


}

int Board::ExistaCarti(int nr_player)
{
	return players[nr_player].cards.size();
}
bool Board::CheckSuma(std::vector<int>elemente, int nr_player, int nr)
{
	int total = GetPlayerCardValue(players[nr_player], nr);
	int suma = 0;
	for (int i = 0; i < elemente.size(); i++)
		suma = suma + cartile_de_jos[elemente[i]].GetValue();
	if (suma == total)
		return true;
	else
		return false;

}
int Board::GetPlayerCardValue(Player player, int nr)
{
	return player.cards[nr].GetValue();

}
void Board::Puncte(int cartea_din_mana, std::vector<int>& deLuat, int nr_player)
{
	players[nr_player].PointCard(players[nr_player].cards[cartea_din_mana]); // pune cartea la gramada
	players[nr_player].RemoveCard(cartea_din_mana); // o sterge de pe jos


	int nr = 0;
	for (int i = 0; i < deLuat.size(); i++)
	{
		players[nr_player].PointCard(cartile_de_jos[deLuat[i] - nr]);
		std::wcout << endl << "Se sterge cartea: " << deLuat[i];
		removeBoardCard(deLuat[i] - nr);
		nr++;

	}
	Calcul(players[nr_player]);
	players[nr_player].GramadaPlayer();

}

void Board::Calcul(Player player)
{
	std::wcout << "Gramada este de marimea:" << player.gramada.size();

}
void Board::DeckTOboard(int nr_carti)
{
	for (int i = 0; i < nr_carti; i++)
	{
		cartile_de_jos.push_back(deck[deck.size() - 1]);
		deck.pop_back();
	}

}

void Board::Trage6Carti(int nr_player)
{
	if (deck.size() > 6)
	{
		for (int i = 0; i < 6; i++)
			Trage(players[nr_player]);
	}
	else
	{
		for (int i = 0; i < deck.size(); i++)
			Trage(players[nr_player]);
	}
}

void Board::DropCard(int nr_player, int element)
{
	cartile_de_jos.push_back(players[nr_player].cards[element]);
	players[nr_player].RemoveCard(element);
	std::wcout << "Cartea jucatorului: " << nr_player + 1 << " " << element;

}

void Board::Refuz()
{
	std::wcout << "Doriti sa schimbati cartile din mana?   [0] - NU  [1] - DA " << endl << endl;
	int raspuns;
	cin >> raspuns;
	if (raspuns != 1 && raspuns != 0)
	{
		std::wcout << "Raspundeti cu 1/0!  ";
		Refuz();
	}
	else if (raspuns == 1)
	{
		for (int i = 0; i < 4; i++)
		{
			cartile_de_jos.push_back(players[0].cards[i]);

		}

		players[0].RemoveAllCard();
		std::wcout << " ramase: ";


		for (int i = 0; i < 6; i++)
		{
			Trage(players[0]);            
		}


		players[0].CartiPlayer();
		std::wcout << endl << endl;
		for (int i = 0; i < cartile_de_jos.size(); i++)
		{
			std::wcout << "Cartea cu numarul: " << i + 1 << " este: " << endl;
			std::wcout << " ____" << endl;
			std::wcout << "|    |" << endl;
			std::wcout << "| ";
			cartile_de_jos[i].AfisareCarte();
			std::wcout << " |" << endl;
			std::wcout << "|____|";
			std::wcout << endl;
		}

	}
	else
	{
		for (int i = 1; i < players.size(); i++)
		{
			Trage(players[i]);
			Trage(players[i]);
		}
	}
	DeckTOboard(4);

}

