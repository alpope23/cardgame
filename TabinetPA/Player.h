#pragma once
#include <iostream>
#include <vector>
#include<tuple> // for tuple 
#include "Card.h"

using namespace std;

class Player {
public:

	string name;
	std::vector<Card>cards;
	std::vector<Card>gramada;
	int score;

public:
	std::tuple <std::wstring, std::wstring, int> data;
	Player(std::string name, std::vector<Card>cards, int score);
	Player();
	~Player();

	void CartiPlayer();
	void SetCards(std::vector<Card>cards);
	void SetName(std::string name);
	void SetScore(int score);
	std::vector<Card>GetCards();
	std::string GetName();
	void AddCard(Card carte);
	void RemoveCard(int element);
	void ShowCards(); 
	void RemoveAllCard();
	void PointCard(Card carte);
	void GramadaPlayer();
	int GetScore();

	void SetDataPlayer();




};

