﻿#include "Card.h"


Card::Card(int valueNew, std::wstring simbolNew)
{
	this->value = valueNew;
	this->simbol = simbolNew;
}
int Card::GetValue()
{
	return value;
}

std::wstring Card::GetSimbol()
{
	return simbol;

}


void Card::AfisareCarte()
{

	std::string sv;
	
	if (value == 12)
	{
		std::wcout << "J"; std::wcout << simbol;
	}
	if (value == 13)
	{
		std::wcout << "Q"; std::wcout << simbol;
	}
	if (value == 14)
	{
		std::wcout << "K"; std::wcout << simbol;
	}
	if (value == 1 || value==11)
	{
		std::wcout << "A"; std::wcout << simbol;
	}
	if (value != 11 && value != 12 && value != 13 && value != 1 && value != 14 )
	{
		std::wcout << value; std::wcout << simbol;
	}

}


void Card::SetValue(int valueNew)
{
	value = valueNew;
}
void Card::SetSimbol(std::wstring simbolNew)
{
	simbol = simbolNew;
}

Card::~Card()
{
}