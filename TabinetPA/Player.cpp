﻿#include "Player.h"
#include "Card.h"
#include "Board.h"

Player::Player() {}

Player::Player(std::string name, std::vector<Card>cards, int score)
{
	this->name = name;
	this->cards = cards;
	this->score = score;

}

void Player::SetName(std::string name)
{
	this->name = name;
}
void Player::SetCards(std::vector<Card>cards)
{
	this->cards = cards;
}

void Player::SetScore(int score)
{
	this->score = score;
}


std::string Player::GetName()
{
	return name;
}

std::vector<Card> Player::GetCards()
{
	return cards;
}


Player::~Player()
{
}
void Player::AddCard(Card carte)
{
	cards.push_back(carte);
}
void Player::RemoveCard(int element)
{
	cards.erase(cards.begin() + element);

}
void Player::RemoveAllCard()
{

	cards.erase(cards.begin() , cards.end());
}
int Player::GetScore()
{
	int suma = 0;
	for (int i = 0; i < gramada.size(); i++)
	{
		if(gramada[i].GetValue()>9)
		suma = suma + 1;
		if (gramada[i].GetValue() == 10 && gramada[i].GetSimbol()==R)
		suma = suma + 2;
		if(gramada[i].GetSimbol()==T)
		suma = suma + 1;
		if (gramada[i].GetValue() == 2 && gramada[i].GetSimbol() == T)
		suma = suma + 2;
		if (gramada[i].GetValue() == 99)  // TABLA
		suma = suma + 1;
		if (gramada[i].GetValue() == 100)  
        suma = suma + 3;
		

	}




	return suma;


}



void Player::ShowCards()
{

	std::wcout << L"║"<<endl;	
	std::wcout << L"║" <<endl;
	std::wcout << L"║" << "          ";   
	for (int i = 0; i < cards.size(); i++)
	{
		std::wcout <<"Cartea " << i << ":          ";
	}
	std::wcout << endl << L"║";

	for (int i = 0; i < cards.size(); i++)
	{
		std::wcout << "             ____  ";

	}
	std::wcout << endl << L"║";
	for (int i = 0; i < cards.size(); i++)
	{
		std::wcout << "            |    | ";
	}
	std::wcout << endl << L"║" << "            ";
	for (int i = 0; i < cards.size(); i++)
	{
		if (cards[i].GetValue() != 10)
		{
			std::wcout << "| ";
			cards[i].AfisareCarte();
			std::wcout << " |             ";
		}
		else
		{
			std::wcout << "| ";
			cards[i].AfisareCarte();
			std::wcout << "|             ";
		}
	}
	std::wcout << endl << L"║" << "            ";
	for (int i = 0; i < cards.size(); i++)
	{
		std::wcout << "|    |             ";
	}
	std::wcout << endl << L"║" << "            ";

	for (int i = 0; i < cards.size(); i++)
	{
		std::wcout << "|____|             ";
	}
	std::wcout << endl;
	std::wcout << L"║" << endl;
	std::wcout << L"║" << endl;


	

}
void Player::SetDataPlayer()
{
	std::wstring n, p;
	int v;
	std::wcout << "Care este Numele tau si ce varsta ai? ";
	std::wcin >> n >> p >> v;
	data = make_tuple(n,p,v);
	std::wcout << get<0>(data) << " " << get<1>(data)<< " " << get<2>(data) << endl;

}

void  Player::CartiPlayer()
{
	for (int i = 0; i < cards.size(); i++)
	cards[i].AfisareCarte();

}
void  Player::GramadaPlayer()
{
	for (int i = 0; i < gramada.size(); i++)
		gramada[i].AfisareCarte();

}
void Player::PointCard(Card carte)
{
	gramada.push_back(carte);

}
