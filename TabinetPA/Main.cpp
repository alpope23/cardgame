#include <iostream>
#include <io.h>
#include <fcntl.h>
#include <windows.h> // for sleep()

#include "Play.h"
#define IR L"\u2665"
#define R L"\u2666"
#define IN L"\u2660"
#define T L"\u2663"


int main()
{
	std::vector<int> secunde{ 1,2,3,4,5,6,7 };
	std::wcout << "Jocul se incarca, asteptati";

	for (auto& num : secunde)
	{
		Sleep(100);
		std::wcout << "." << std::flush;
		Sleep(100);
		std::wcout << "." << std::flush;
		Sleep(100);
		std::wcout << "." << std::flush;
		Sleep(100);
		std::wcout << "\b\b\b   \b\b\b" << std::flush;
	}
	std::wcout << endl;
	_setmode(_fileno(stdout), _O_U16TEXT);

	Play joc;

	joc.Start();


	

}

