#pragma once
#include <io.h>
#include <fcntl.h>
#include <iostream>
#include <ctime>
#include <vector>
#include <memory>
#include <stdio.h>
#include <algorithm>
#define IR L"\u2665"
#define R L"\u2666"
#define IN L"\u2660"
#define T L"\u2663"

class Card
{

	int value;
	std::wstring simbol;

public:
	//Card& operator=(const Card& other);
	Card(int valueNew, std::wstring simbolNew);
	Card();
	void SetValue(int valueNew);
	void SetSimbol(std::wstring simbolNew);
	int GetValue();
	std::wstring GetSimbol();
	void AfisareCarte();
	Card& operator =(const Card& newCard)
	{
		this->simbol = newCard.simbol;
		this->value = newCard.value;
		return *this;
	}

	~Card();



};


