#pragma once
#include <iostream>
#include <windows.h> // for sleep()
#include "Board.h"
#include "Player.h"
#include "Card.h"
#include "Play.h"


Play::Play()
{

}
void Play::Start()
{
	
	/*	std::wcout << "Loading Please Wait";
		while (true)
		{
			Sleep(1);
			std::wcout << "." << std::flush;
			Sleep(1);
			std::wcout << "." << std::flush;
			Sleep(1);
			std::wcout << "." << std::flush;
			Sleep(1);
			std::wcout << "\b\b\b   \b\b\b" << std::flush;
		}
		
	*/
	std::wcout << "===================================================================================================================="<<endl;
	std::wcout << "                                                        DISTRACTIE PLACUTA                   "<<endl;
	std::wcout << "===================================================================================================================="<<endl;
	Board pachet1;

	pachet1.Deck();
	pachet1.ShufflePachet();
	pachet1.ImparteCartile();
	pachet1.AfisareMaini();
	pachet1.Refuz();
	pachet1.AfisareMaini();
	pachet1.AfisareBoard();

	Joc(pachet1);
}
void Play::Joc(Board& pachet)
{
	int nr_tura = 0;

	while (pachet.StillPlayable() < pachet.GetDeckSize())
	{
		nr_tura++;
		pachet.AfisareMaini();
		pachet.AfisareBoard();
		for (int i = 0; i < pachet.GetNrPlayeri(); i++)
		{

			pachet.AfisareMaini();
			pachet.AfisareBoard();
			if (pachet.ExistaCarti(i) == 0)
			{
				

				pachet.Trage6Carti(i);

				pachet.AfisareMaini();
				pachet.AfisareBoard();

				Tura(i, pachet); pachet.AfisareMaini();
				pachet.AfisareBoard();

			}
			else
			{
				Tura(i, pachet);
				pachet.AfisareMaini();
				pachet.AfisareBoard();
			}

		}
	}
	std::wcout << "=========================================================="<<endl;
	std::wcout << "                         FINAL DE JOC                     "<<endl;
	std::wcout << "=========================================================="<<endl;
	pachet.Winner();



}

void Play::Tura(int index, Board& pachet)
{

	int carte_mana;
	vector<int> carti_de_jos;
	std::wcout << "test" << pachet.getNrMaini(index) << endl << endl;

	if (pachet.getNrMaini(index) > 0)
	{

		std::wcout << "Jucatorule " << index + 1 << " Cu ce carte din mana vrei sa faci suma? ";
		std::wcout << "Alegeri disponibile: ";
		while (pachet.getNrMaini(index) > 0)
		{
			pachet.PerechiMana(index);
			std::wcout << endl << endl;
			Alege(pachet, index);
			if (pachet.GetSizeCartiJos() == 0)
			{
				pachet.AdaugaCarteExtra2(index);
			}


			std::wcout << endl << endl;
		}
	}
	else
	{

		std::wcout << endl << endl;

		std::wcout << "Jucatorule " << index + 1;
		std::wcout << " nu exista alegeri disponibile, arunca o carte" << endl;
		int nr;
		std::wcout << " Ce carte vrei sa arunci? " << endl;
		std::cin >> nr;
		pachet.DropCard(index, nr);
		if (pachet.GetSizeCartiJos() == 0)
		{
			pachet.AdaugaCarteExtra2(index);
		}



	}




}

void Play::Alege(Board& pachet, int nr_player)

{

	int cartea_din_mana, cartea_de_jos, cartea_de_jos2;
	std::wcout << "Exista atatea carti in mana: " << pachet.ExistaCarti(nr_player) << endl;
	std::wcout << "Ce carte din mana doriti sa schimbati?";
	std::cin >> cartea_din_mana;
	int numar;
	std::vector<int>deLuat;
	std::wcout << "Cate carti vrei sa iei de jos? " << std::endl;
	std::cin >> numar;
	while (numar < 1 || numar > 5)
	{
		std::wcout << "Prea multe carti/ Sau deloc! Introduceti din nou!" << std::endl;
		std::cin >> numar;
	}
	std::wcout << "Ce carti " << std::endl;

	int aux;
	for (int index = 0; index < numar; index++)
	{
		std::cin >> aux;
		deLuat.push_back(aux);
	}
	for (int index = 0; index < numar; index++)
	{
		std::wcout << deLuat[index] << " ";
	}

	if (pachet.CheckSuma(deLuat, nr_player, cartea_din_mana) == true)
	{
		pachet.Puncte(cartea_din_mana, deLuat, nr_player);
	}
	else
	{
		std::wcout << endl << "INTRODU ALTE DATE!";

	}




}
